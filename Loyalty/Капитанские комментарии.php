<?php
$customerId = $data->customer->id ?? 0;
if (!$customerId) {
    // Customer ID is not present in the webhook data
    return;
}
try {
    $this->updateOrCreateCustomer($data->customer, $shop->id);
    $this->customerRepository->checkAndSendInvite($customerId); // Try to send shopify-invite

    $customer = $this->customer->find($customerId);
    if (!$customer->first_purchase_noticed) {
        $customer->first_purchase_noticed = true;
        $customer->save();
        $this->emailRepository->sendFirstPurchase(
            $shop->id,
            $customerId
        );
    }
} catch (\Exception $e) {
    \Log::error($e->getMessage(), ['trace' => $e->getTraceAsString()]);
}

/*if (isset($data->tags) && $this->orderHasTag($data->tags)) {
    // Order has a tag "spurit-loyalty-points", points was charged on ORDERS_CREATE webhook
    return;
}*/

if (!$shop->config->active) {
    // Config is disabled for current shop
    return;
}

if (!$shop->config->purchase->active) {
    // Purchase config is disabled for current shop
    return;
}

if (strcasecmp($data->financial_status, self::ORDER_STATUS_PAID) !== 0) {
    // For paid orders expected status is "paid"
    return;
}

if ($shop->config->referral->active) {
    $this->topUpReferrer($customerId, $shop->config);
}

$pointsToTopup = $this->getPointsToTopup($data, $shop->config);
if (!$pointsToTopup) {
    // No points to topoup
    return;
}

?>