<?php

$ids = $rules->sort(function (Rule $rule1, Rule $rule2) use ($now): bool {
    $ruleTime1 = $rule1->nextStartTime($now);
    $ruleTime2 = $rule2->nextStartTime($now);
    return $ruleTime1->getTimestamp() !== $ruleTime2->getTimestamp() ?
        $ruleTime1 > $ruleTime2 :
        ($rule1->enabled !== $rule2->enabled ?
            $rule1->enabled < $rule2->enabled :
            strtolower($rule1->name) > strtolower($rule2->name)
        );
})->pluck('id')->all();


/*
 * Этот кусок вообще продублиброван. Свитч немного урезан для наглядности
 */
switch ($field) {
    case 'start_time':
        $rules = $query->get();
        if (!$rules->count()) {
            return $query;
        }
        $ids = $rules->sort(function (Rule $rule1, Rule $rule2) use ($now): bool {
            $ruleTime1 = $rule1->nextStartTime($now);
            $ruleTime2 = $rule2->nextStartTime($now);
            return $ruleTime1->getTimestamp() !== $ruleTime2->getTimestamp() ?
                $ruleTime1 > $ruleTime2 :
                ($rule1->enabled !== $rule2->enabled ?
                    $rule1->enabled < $rule2->enabled :
                    strtolower($rule1->name) > strtolower($rule2->name)
                );
        })->pluck('id')->all();
        return $query->orderBy(
            \DB::raw(sprintf('FIELD(`id`, %s)', implode(',', $ids)))
        );

    case 'end_time':
        $rules = $query->get();
        if (!$rules->count()) {
            return $query;
        }
        $ids = $rules->sort(function (Rule $rule1, Rule $rule2) use ($now): bool {
            $ruleTime1 = $rule1->nextEndTime($now);
            $ruleTime2 = $rule2->nextEndTime($now);
            return $ruleTime1->getTimestamp() !== $ruleTime2->getTimestamp() ?
                $ruleTime1 > $ruleTime2 :
                ($rule1->enabled !== $rule2->enabled ?
                    $rule1->enabled < $rule2->enabled :
                    strtolower($rule1->name) > strtolower($rule2->name)
                );
        })->pluck('id')->all();
        return $query->orderBy(
            \DB::raw(sprintf('FIELD(`id`, %s)', implode(',', $ids)))
        );
}