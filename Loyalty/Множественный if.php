<?php
if($filter['key'] == 'createdOnAfter') {
    $date = Carbon::parse($filter['value'])->format('Y-m-d H:i:s');
    $query = $this->filterByCreated($query, $date);
}
if($filter['key'] == 'createdOnBefore') {
    $date = Carbon::parse($filter['value'])->format('Y-m-d H:i:s');
    $query = $this->filterByCreated($query, $date, '<=');
}
if($filter['key'] == 'locationFilter') {
    $countriesArrayFilter[] = Helpers::getCodeByCountry($filter['value']);
}
if($filter['key'] == 'balanceGreaterFilter') {
    $query = $this->filterByBalance($query, (int)$filter['value'], '>');
}
if($filter['key'] == 'balanceLowerFilter') {
    $query = $this->filterByBalance($query, (int)$filter['value'], '<');
}
if($filter['key'] == 'spentGreaterFilter') {
    $query = $this->filterBySpent($query, (int)$filter['value'], '>');
}
if($filter['key'] == 'spentLowerFilter') {
    $query = $this->filterBySpent($query, (int)$filter['value'], '<');
}

/*
 * Приходится вчитываться в код, чтобы понять, должно условие выполниться 2 раза или нет
 * Свич можно свернуть
 * У свич есть поведение по умолчанию
 *
 * Минусы свитча:
 * дополнительная строчка для break
 * свитч сравнивает с преобразованием типов
 */




switch ($filter['key']) {
    case 'createdOnAfter': {
        $date = Carbon::parse($filter['value'])->format('Y-m-d H:i:s');
        $query = $this->filterByCreated($query, $date);
        break;
    }
    case 'createdOnBefore': {
        $date = Carbon::parse($filter['value'])->format('Y-m-d H:i:s');
        $query = $this->filterByCreated($query, $date, '<=');
        break;
    }
    case 'locationFilter': {
        $countriesArrayFilter[] = Helpers::getCodeByCountry($filter['value']);
        break;
    }
    case 'balanceGreaterFilter': {
        $query = $this->filterByBalance($query, (int)$filter['value'], '>');
        break;
    }
    case 'balanceLowerFilter': {
        $query = $this->filterByBalance($query, (int)$filter['value'], '<');
        break;
    }
    case 'spentGreaterFilter': {
        $query = $this->filterBySpent($query, (int)$filter['value'], '>');
        break;
    }
    case 'spentLowerFilter': {
        $query = $this->filterBySpent($query, (int)$filter['value'], '<');
        break;
    }
    default: {
        Log::error(sprintf('Unknown filter %s', $filter['key']));
    }
}