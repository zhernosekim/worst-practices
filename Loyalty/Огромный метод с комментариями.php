<?php

class Test
{

    /**
     * @param \stdClass $data
     * @param Models\Shop\Shop $shop
     */
    public function create(\stdClass $data, Models\Shop\Shop $shop): void
    {
//    $customerId = $data->customer->id ?? 0;                           //Этот кусок кода я закомментировал, тут валидация
//    if (!$customerId) {
//        // Customer ID is not present in the webhook data
//        return;
//    }
//
//    $this->updateOrCreateCustomer($data->customer, $shop->id);
//
//    if (!isset($data->tags) || !$this->orderHasTag($data->tags)) {
//        // Order does not have a tag "spurit-loyalty-points"
//        return;
//    }
//
//    if (!$shop->config->active) {
//        // Config is disabled for current shop
//        return;
//    }
//
//    $pointsToCharge = $this->getPointsToCharge($data->tags);
//    if (!$pointsToCharge) {
//        // Points amount to charge is not present in the Order data
//        return;
//    }

        // Charge points
        $this->balanceRepository->charge(
            $pointsToCharge,
            $customerId,
            'Payment for order with points',
            $data->id
        );

        // Saving stats
        $this->statsRepository->saveOrderInfo(
            $data->id,
            $data->name,
            floatval($data->total_price),
            $shop->id
        );
        $products = [];
        foreach ($data->line_items as $line_item) {
            $products[$line_item->product_id] = $line_item;
        }
        foreach ($products as $product) {
            $this->statsRepository->saveProductInfo(
                $product->product_id,
                $product->title,
                floatval($product->price),
                $shop->id
            );
        }

        // Notice to customer about his points was charged
        $this->emailRepository->sendSpend(
            $shop->id,
            $customerId,
            [
                'pointsDebited' => $pointsToCharge,
            ]
        );

        // Points charged
        return;
    }

}