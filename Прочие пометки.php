Неформальные процедуры обзоров передавались
от человека человеку в общей культуре программирования
задолго до того, как информация об этом стала появляться
в печатных материалах. Необходимость обзоров была
настолько очевидна лучшим программистам, что они
редко упоминали об этом в статьях и книгах,
тогда как худшие программисты считали, что они
настолько хороши, что их работа не нуждается в обзорах.

Дениел Фридмен и Джеральд Вайнберг (авторы книги по техническому код-ревью)

****************************************************
****************************************************

Расширение для Шторма PhpClean умеет проверять указаны ли типы в методах и возвращаемые типы
