<?php

class IfIfIf {
    public function findTargetProductsAndVariants(): array
    {
        if (!$this->exists) {
            throw new \RuntimeException('Can\'t find variants for nonexistent rule');
        }
        $products = [];
        if ($this->apply_to === self::APPLY_TO_COLLECTIONS) {
            $collectionIds = $this->collections()->pluck('collection_id');
            $allProducts = [];
            foreach ($collectionIds as $cid) {
                $allProducts += $this->getMappingProductWithVariantsFromShopify($cid);
            }
            $uniqueProductIds = array_unique(array_keys($allProducts));
            foreach ($uniqueProductIds as $uniqueProductId) {
                $products[$uniqueProductId] = $allProducts[$uniqueProductId];
            }
        } else {
            if ($this->apply_to === self::APPLY_TO_PRODUCTS) {
                $uniqueProductIds = $this->variants()->pluck('product_id')->unique()->toArray();
                $productVariantIds = $this->variants()->select(['product_id', 'variant_id'])->get()->toArray();
                $products = $this->getMappingProductWithVariants($uniqueProductIds, $productVariantIds);

                $productIds = $this->products()->pluck('product_id')->toArray();
                if (!empty($productIds)) {
                    $products += $this->getMappingProductWithVariantsFromShopify(0, $productIds);
                }
            } else {
                if ($this->apply_to === self::APPLY_TO_STORE) {
                    $products = $this->getMappingProductWithVariantsFromShopify();
                } else {
                    throw new \RuntimeException(sprintf(
                        'Unexpected value of $rule->apply_to: %s',
                        $this->apply_to
                    ));
                }
            }
        }

        return $products;
    }
}