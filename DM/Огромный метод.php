<?php


class BigMethod {
    /**
     * Взято из комманд
     * 164 символа
     * @param int $migrationShopId
     */
    private function createAndEnableRules(int $migrationShopId): void
    {
        $processingDiscounts = $this->oldDbConnection->table('discounts')
            ->where('shop_id', $migrationShopId)
            ->whereIn('status', [self::DISCOUNT_STARTING, self::DISCOUNT_FINISHING])
            ->count();
        if ($processingDiscounts) {
            throw new Exceptions\ProcessingDiscountException();
        }

        $discounts = $this->oldDbConnection->table('discounts')
            ->where('shop_id', $migrationShopId)
            ->get();

        $discounts->each(function ($discount) {
            $discountsProducts = $this->oldDbConnection->table('discounts_products')
                ->where('discount_id', $discount->id)
                ->get();
            $discountsCategories = $this->oldDbConnection->table('discounts_categories')
                ->where('discount_id', $discount->id)
                ->get();

            // Save rule
            /**
             * @var Rule $rule
             */
            $rule = $this->shop->rules()->getModel()->newInstance();
            $rule->name = $discount->title;
            $rule->priority = $this->getNextPriority();
            $rule->discount = $discount->discount;
            $rule->discount_type = $discount->rounding_type === 'fixed' ? Rule::DISCOUNT_FIXED : Rule::DISCOUNT_PERCENTAGE;
            $rule->apply_to = $discountsCategories->count() ? Rule::APPLY_TO_COLLECTIONS : Rule::APPLY_TO_PRODUCTS;
            $rule->created_at = $discount->created;

            if ($discount->rounding) {
                $rule->rounding_on = true;
                $rule->rounding_value = (float) ('0.' . $discount->rounding);
            }
            switch ($discount->state) {
                case 'active': $rule->enabled = true; break;
                case 'not active': $rule->enabled = false; break;
                case 'deleted': $rule->deleted_at = Carbon::now(); break;
                default: throw new \RuntimeException('Unknown type of state property');
            }

            $this->shop->rules()->save($rule);

            // Save schedule
            if ($discount->start || $discount->end) {
                /**
                 * @var Schedule $schedule
                 */
                $schedule = $rule->schedule()->getModel()->newInstance();
                $schedule->active = $this->isDiscountEnabled($discount);
                $schedule->type = Schedule::TYPE_CUSTOM;
                $schedule->weekly_day_start = 1;
                $schedule->weekly_day_end = 7;
                $schedule->monthly_day_start = 1;
                $schedule->monthly_day_end = 31;
                if ($discount->start) {
                    $schedule->time_start = Carbon::parse($discount->start)->format('H:i');
                    $schedule->custom_date_start = Carbon::parse($discount->start)->format('Y-m-d');
                }
                if ($discount->end) {
                    $schedule->time_end = Carbon::parse($discount->end)->format('H:i');
                    $schedule->custom_date_end = Carbon::parse($discount->end)->format('Y-m-d');
                }
                $rule->schedule()->save($schedule);
            }

            // Save collections
            $collections = [];
            foreach ($discountsCategories ?? [] as $category) {
                /**
                 * @var Collection $collection
                 */
                $collection = $rule->collections()->getModel()->newInstance();
                $collection->collection_id = $category->cid;
                $collection->name = $this->getCollectionTitle($category->cid);
                $collections[] = $collection;
            }
            $rule->collections()->saveMany($collections);

            // Save tags
            $tags = [];
            if ($discount->tag) {
                foreach (explode(',', $discount->tag) as $discountTag) {
                    /**
                     * @var Tag $tag
                     */
                    $tag = $rule->tags()->getModel()->newInstance();
                    $tag->tag = $discountTag;
                    $tags[] = $tag;
                }
                $rule->tags()->saveMany($tags);
            }

            // Save products and variants
            $appliedProducts = [];
            $appliedVariants = [];
            $shopifyProducts = [];
            $uniqueProductIds = $discountsProducts->pluck('pid')->unique()->toArray();
            $productVariantIds = $discountsProducts->map(function(\stdClass $p) {
                return ['pid' => $p->pid, 'vid' => $p->vid];
            })->toArray();
            $productVariantsDictionary = $this->createProductVariantsDictionary($uniqueProductIds, $productVariantIds);
            foreach ($productVariantsDictionary as $productId => $variantIds) {
                try {
                    $shopifyProduct = $this->shopifyApi->products()->getSingle($productId, ['fields' => 'id,title,variants']);
                    $shopifyProducts[] = $shopifyProduct;
                } catch (\Exception $e) {
                    Log::info($e->getMessage());
                    continue;
                }
                if (count($shopifyProduct->variants) === count($variantIds)) {
                    /**
                     * @var Product $product
                     */
                    $product = $rule->products()->getModel()->newInstance();
                    $product->product_id = $productId;
                    $product->name = $shopifyProduct->title ?? '';
                    $product->name = substr($product->name, 0, 191);

                    $appliedProducts[] = $product;
                } else {
                    foreach ($variantIds as $variantId) {
                        /**
                         * @var Variant $variant
                         */
                        $variant = $rule->variants()->getModel()->newInstance();
                        $variant->product_id = $productId;
                        $variant->variant_id = $variantId;
                        $appliedVariants[] = $variant;
                    }
                }
                $this->applyTags($rule, $productId);
            }

            // Save applied prices and applied tags
            if (!$collections && $uniqueProductIds) {

                $rule->products()->saveMany($appliedProducts);
                $rule->variants()->saveMany($appliedVariants);
                if ($this->isDiscountEnabled($discount)) {
                    $this->applyProducts($rule, $shopifyProducts, $productVariantsDictionary);
                }
            } else if ($collections && $uniqueProductIds) {
                $duplicateRule = $this->duplicateRule($rule);
                $duplicateRule->apply_to = Rule::APPLY_TO_PRODUCTS;
                $duplicateRule->save();
                $duplicateRule->products()->saveMany($appliedProducts);
                $duplicateRule->variants()->saveMany($appliedVariants);

                if ($this->isDiscountEnabled($discount)) {
                    $this->applyProducts($duplicateRule, $shopifyProducts, $productVariantsDictionary);
                    $this->applyCollections($rule, $collections);
                }
            }  else {
                //$collections && !$uniqueProductIds
                if ($this->isDiscountEnabled($discount)) {
                    $this->applyCollections($rule, $collections);
                }
            }
        });
    }

    /**
     * 123 символа
     * @return int
     */
    public function handle(): int
    {
        $this->logCronStart();

        foreach ($this->shop->all() as $shop) {
            if (!$shop->rules()->applied()->exists()) {
                continue;
            }

            try {
                $counter = 0;
                $this->cacheCatalog->checkForUpdates(
                    $shop,
                    function (int $productId) use ($shop, &$counter): void {
                        $counter++;

                        $products = $this->rule->getMappingProductWithVariantsFromShopify(0, [$productId]);
                        // Reapply rules for products and variants
                        foreach ($products as $productId => $variantIds) {
                            $rollBackedVariantIds = [];
                            foreach ($variantIds as $variantId) {
                                $rule = $this->rule->findForProductAndVariant($shop, $productId, $variantId);
                                if ($rule) {
                                    $rule->applyForProduct($productId, [$variantId]);
                                    $this->cacheProductRule[$productId] = $rule;
                                } else {
                                    $rollBackedVariantIds[] = $variantId;
                                }
                            }

                            if (!empty($rollBackedVariantIds)) {
                                $this->rule->rollbackForProduct($productId, $rollBackedVariantIds);
                            }
                        }

                        $this->rule->productChanger->submit(
                            function (\stdClass $response) {
                                if (isset($response->product)) {
                                    $productId = $response->product->id;

                                    if (isset(Rule\Rule::$changedAppliedPrices[$productId])) {
                                        $rule = $this->cacheProductRule[$productId];
                                        $variantIds = array_keys(Rule\Rule::$changedAppliedPrices[$productId]);
                                        $rule->deleteAppliedByProduct($productId, $variantIds);
                                        $rule->rollbackSkippedProducts($response->product, $variantIds);

                                        $appliedPrices = array_values(Rule\Rule::$changedAppliedPrices[$productId]);
                                        $rule->appliedPrices()->saveMany($appliedPrices);
                                        $rule->shop()->increment('operations', count($appliedPrices));
                                    }

                                    if (isset(Rule\Rule::$rollBackedProducts[$productId])) {
                                        $this->rule->deleteAppliedByProduct($productId, Rule\Rule::$rollBackedProducts[$productId]);
                                        $this->rule->deleteSkippedProducts($response->product, Rule\Rule::$rollBackedProducts[$productId]);
                                    }

                                } else if (isset($response->data)) {
                                    $responseData = (array)$response->data;
                                    foreach ($responseData as $entry) {
                                        if (isset($entry->productVariant)) {
                                            $parts = explode('/', $entry->productVariant->product->id);
                                            $productId = end( $parts);
                                            $parts = explode('/', $entry->productVariant->id);
                                            $variantId = end($parts);

                                            //TODO can't increment operations because shop is not defined, when prices are revoked
                                            $this->rule->appliedPricesModel->where('variant_id', $variantId)->delete();
                                            $this->rule->skippedProductModel->where('variant_id', $variantId)->delete();

                                            if (
                                                isset(Rule\Rule::$changedAppliedPrices[$productId]) &&
                                                isset(Rule\Rule::$changedAppliedPrices[$productId][$variantId])
                                            ) {
                                                $rule = $this->cacheProductRule[$productId];
                                                $rule->appliedPrices()->save(Rule\Rule::$changedAppliedPrices[$productId][$variantId]);
                                                $rule->shop()->increment('operations');
                                            }
                                        } else {
                                            $parts = explode('/', $entry->product->id);
                                            $productId = end($parts);

                                            $this->rule->appliedTagsModel->where('product_id', $productId)->delete();
                                            if (isset(Rule\Rule::$changedAppliedTags[$productId])) {
                                                $rule = $this->cacheProductRule[$productId];
                                                $rule->appliedTags()->saveMany(Rule\Rule::$changedAppliedTags[$productId]);
                                            }
                                        }
                                    }
                                }
                            }
                        );
                    }
                );
                if ($counter > 0) {
                    event(new OnSettingsSaved($shop));
                }

            } catch (\Exception $e) {
                $this->error($e);
                \Log::error($e);
                if ($e instanceof ShopifyApiException) {
                    if ($e->getCode() === 404) {
                        $shop->cacheCatalog()->delete();
                        $shop->installed = false;
                        $shop->save();
                        $shop->delete();

                        \Log::info(sprintf(
                            'Shop deleted user ID %s',
                            $shop->user_id
                        ));
                    }
                    if ($e->getCode() === 402 || $e->getCode() === 404) {
                        continue;
                    }
                }
                Sentry::sendExceptionAsWarning($e);
                continue;
            }
        }

        $this->logCronStop();
        return 0;
    }

    /**
     * Метод не из команды - из модели Rule
     * @param callable|null $onCounterChange
     * @throws \Exception
     */
    public function apply(callable $onCounterChange = null): void
    {
        if (!$this->exists) {
            throw new \RuntimeException('Can\'t apply nonexistent rule');
        }
        if (!$this->isEnabledAndActive()) {
            throw new WrongRuleStateException('Can\'t apply disabled or inactive rule');
        }
        $this->setupApi($this->shop);
        $this->skippedProducts()->delete();

        $products = $this->findTargetProductsAndVariants();
        if ($this->apply_to === self::APPLY_TO_COLLECTIONS) {
            $this->cacheCatalog->preloadProducts($this->shop, true, ...array_keys($products));
        }

        $processedVariantIds = [];
        foreach ($products as $productId => $variantIds) {
            if ($this->apply_to === self::APPLY_TO_COLLECTIONS) {
                $this->cacheCatalog->getProductCollections($this->shop, $productId);
            }
            $appliedVariantIds = [];
            foreach ($variantIds as $variantId) {
                $isApplied = $this->appliedPricesModel
                    ->where('variant_id', $variantId)
                    ->where('rule_id', '!=', $this->id)
                    ->exists();
                if (!$isApplied) {
                    $appliedVariantIds[] = $variantId;
                }
            }

            $addSkippedVariants = function ($skippedVariants) use ($productId) {
                foreach ($skippedVariants as $skippedVariant) {
                    /**
                     * @var Shopify\SkippedProduct $skippedProduct
                     */
                    $skippedProduct = $this->skippedProducts()->getModel()->newInstance();
                    $skippedProduct->product_id = $productId;
                    $skippedProduct->variant_id = $skippedVariant;
                    $this->skippedProducts()->save($skippedProduct);
                }
            };
            try {
                if (!empty($appliedVariantIds)) {
                    $this->applyForProduct($productId, $appliedVariantIds);
                    $skippedVariants = array_diff($variantIds, $appliedVariantIds);
                    call_user_func($addSkippedVariants, $skippedVariants);
                } else {
                    $product = $this->getProduct($productId);
                    if (count($product->variants) === count($variantIds)) {
                        /**
                         * @var Shopify\SkippedProduct $skippedProduct
                         */
                        $skippedProduct             = $this->skippedProducts()->getModel()->newInstance();
                        $skippedProduct->product_id = $productId;
                        $this->skippedProducts()->save($skippedProduct);
                    } else {
                        call_user_func($addSkippedVariants, $variantIds);
                    }
                }
            } catch (\Exception $e) {
                if ($e instanceof ShopifyApiException && $e->getCode() === 404) {
                    app(Webhooks\Product::class)->delete($this->shop, (object)['id' => $productId]);
                }
            }
            $processedVariantIds = array_merge($processedVariantIds, $appliedVariantIds);
        }

        $counter = 0; // Count successful Product operations
        $productsTotal = sizeof($products);
        $updatedProductIds = [];
        $this->productChanger->submit(
            function (\stdClass $response) use ($onCounterChange, &$counter, $productsTotal, &$updatedProductIds) {
                //apply through REST API
                if (isset($response->product)) {
                    $productId = $response->product->id;
                    if (!in_array($productId, $updatedProductIds)) {
                        $updatedProductIds[] = $productId;
                        $counter++;
                    }

                    $variantIds = array_keys(self::$changedAppliedPrices[$productId]);
                    $this->deleteAppliedByProduct($productId, $variantIds);
                    $this->rollbackSkippedProducts($response->product, $variantIds);

                    $appliedPrices = array_values(self::$changedAppliedPrices[$productId]);
                    $this->appliedPrices()->saveMany($appliedPrices);
                    if (isset(self::$changedAppliedTags[$productId])) {
                        $this->appliedTagsModel->where('product_id', $productId)->delete();
                        $this->appliedTags()->saveMany(self::$changedAppliedTags[$productId]);
                    }

                    $this->shop()->increment('operations', count($appliedPrices));
                    //apply through GraphQL API
                } else if (isset($response->data)) {
                    $responseData = (array)$response->data;
                    foreach ($responseData as $entry) {
                        //save variants
                        if (isset($entry->productVariant)) {
                            $parts = explode('/', $entry->productVariant->product->id);
                            $productId = end( $parts);
                            $parts = explode('/', $entry->productVariant->id);
                            $variantId = end($parts);

                            $this->appliedPricesModel->where('variant_id', $variantId)->delete();
                            $this->skippedProducts()->where('variant_id', $variantId)->delete();
                            $this->appliedPrices()->save(self::$changedAppliedPrices[$productId][$variantId]);
                            $this->shop()->increment('operations');
                            //save tags
                        } else {
                            $parts = explode('/', $entry->product->id);
                            $productId = end($parts);

                            $this->appliedTagsModel->where('product_id', $productId)->delete();
                            $this->appliedTags()->saveMany(self::$changedAppliedTags[$productId]);
                        }
                        if (!in_array($productId, $updatedProductIds)) {
                            $updatedProductIds[] = $productId;
                            $counter++;
                        }
                    }
                }
                if (!is_null($onCounterChange)) {
                    call_user_func($onCounterChange, $counter, $productsTotal);
                }
            }
        );
        self::$changedAppliedPrices = [];
        self::$changedAppliedTags = [];

        $productIdsToRollBack = $this->appliedPrices()
            ->whereNotIn('variant_id', $processedVariantIds)
            ->pluck('product_id')
            ->unique()
            ->toArray();
        $productVariantIds = $this->appliedPrices()->select(['product_id', 'variant_id'])->get()->toArray();
        $products = $this->getMappingProductWithVariants($productIdsToRollBack, $productVariantIds);
        $rollbackProductsTotal = count($products);

        if ($rollbackProductsTotal) {
            $this->rollBackProducts($products);
            $counter += $rollbackProductsTotal;
            $productsTotal += $rollbackProductsTotal;
        }
        if (!is_null($onCounterChange)) {
            call_user_func($onCounterChange, $counter, $productsTotal);
        }

        event(new OnSettingsSaved($this->shop));
    }
}